import json
import random


def process_random_station(station_list, stations_visited=None):
    """Method that tries to find a random station from the ones passed through
    'station_list' - this is the connections list of a certain station

    If no stations_visited is passed, the code will choose randomly from the
    station_list, a staiton that is not closed - cats

    If stations_visited is passed, that the code will try not to pick of those
    station and ignore closed stations. If not possible, it will pick one of
    the stations that are not closed. - owner

    """
    station_list_not_closed = []

    # create list of station connections not closed
    if station_list:
        for station in station_list:
            if not station.closed:
                station_list_not_closed.append(station)

    # check if stations visited is an empty list or None (in case of cats,
    # it is not passed so we don't care); humans are smarter
    if stations_visited:
        stations_not_visited = []
        for station in station_list_not_closed:
            if station not in stations_visited:
                stations_not_visited.append(station)

        # if we have station opened and not visited, pick a random one,
        # else pick a station from the visited ones, if any
        if stations_not_visited:
            return random.choice(stations_not_visited)
    if station_list_not_closed:
        return random.choice(station_list_not_closed)
    else:
        return None


class Station(object):
    """Station object"""
    def __init__(self, uuid, name):
        self.id = uuid
        self.name = name
        self.connections = []
        self.closed = False


class Cat(object):
    """Cat object"""
    def __init__(self, id):
        self.id = id
        self.current_station = None
        self.moves = 0
        self.owner = None

    def pick_random_station(self):
        """Method that will try sort out the next station where
        the cat should go"""
        if self.current_station and self.current_station.connections:
            random_station = process_random_station(
                    self.current_station.connections)
            if random_station:
                self.current_station = random_station
                self.moves += 1

    def is_found(self):
        """Returns cat status, whether it was found by the owner or not"""
        return self.current_station == self.owner.current_station


class Owner(object):
    """Owner object"""
    def __init__(self, id, cat):
        self.id = id
        self.cat = cat
        self.current_station = None
        self.moves = 0
        self.visited_stations = []

    def pick_random_station(self):
        """Method that will try sort out the next station where
        the owner should go"""
        if self.current_station:
            random_station = process_random_station(
                    self.current_station.connections, self.visited_stations)
            if random_station:
                self.current_station = random_station
                self.moves +=1
                if random_station not in self.visited_stations:
                    self.visited_stations.append(random_station)

    def found_cat(self):
        """Returns cat status, whether it was found by the owner or not"""
        return self.current_station == self.cat.current_station


def build_stations_and_connections():
    """Method that read the tfl connections and tfl station json files
    and builds a data structure of stations and their connections and
    returns a dict of Station objects by id"""
    with open('tfl_connections.json') as tfl_conn_file:
        tfl_connections_to_process = json.load(tfl_conn_file)

    # load json tfl stations
    with open('tfl_stations.json') as tfl_stations_file:
        tfl_stations_to_process = json.load(tfl_stations_file)

    tfl_stations_id = {}

    # build tfl new stations structure
    for tfl_station_pr in tfl_stations_to_process:
        new_station = Station(str(tfl_station_pr[0]), str(tfl_station_pr[1]))
        tfl_stations_id[new_station.id] = new_station

    # build connections
    for tfl_connections_pr in tfl_connections_to_process:
        conn1 = str(tfl_connections_pr[0])
        conn2 = str(tfl_connections_pr[1])
        if not tfl_stations_id[conn2] in tfl_stations_id[conn1].connections:
            tfl_stations_id[conn1].connections.append(tfl_stations_id[conn2])
        if not tfl_stations_id[conn1] in tfl_stations_id[conn2].connections:
            tfl_stations_id[conn2].connections.append(tfl_stations_id[conn1])

    return tfl_stations_id

tfl_station_by_id = build_stations_and_connections()
nr_to_generate = int(raw_input("Please enter as an integer how many owner-cat"
                               " instances to generate: "))
cats = []
owners = []
count_generate = 1
# create and distribute randomly cats and owners
while count_generate <= nr_to_generate:
    cat = Cat(count_generate)
    owner = Owner(count_generate, cat)
    cat.owner = owner

    # pick random stations for cat and owner and them create the objects
    cat_station_key = random.choice(tfl_station_by_id.keys())
    tfl_station_choose_from = tfl_station_by_id.keys()
    tfl_station_choose_from.remove(cat_station_key)

    owner_station_key = random.choice(tfl_station_choose_from)

    cat.current_station = tfl_station_by_id[cat_station_key]
    owner.current_station = tfl_station_by_id[owner_station_key]

    owners.append(owner)
    cats.append(cat)

    count_generate += 1

# START THE SEARCH FOR CATS!!!!

total_moves = 100000
current_move = 0

cats_search_list = cats
owners_search_list = owners
nr_of_cats_found = 0
movements_to_find_a_cat_list = []

while current_move < total_moves:
    # move cats
    new_cats_search_list = []
    new_owners_search_list = []
    for owner in owners_search_list:
        if owner.found_cat():
            # we close the station
            owner.current_station.closed = True
            nr_of_cats_found += 1
            movements_to_find_a_cat_list.append(owner.moves)
            print 'Owner '+str(owner.id)+\
                  ' found cat '+str(owner.cat.id)+' - '+\
                  owner.current_station.name+' is now closed'
        else:
            owner.pick_random_station()
            new_owners_search_list.append(owner)

    for cat in cats_search_list:
        if not cat.is_found():
            cat.pick_random_station()
            new_cats_search_list.append(cat)
    # create a new list of owners that didn't find their cats, as well
    # as a list of cats that weren't found - so that we can loop through next
    # time
    owners_search_list = new_owners_search_list
    cats_search_list = new_cats_search_list

    current_move += 1
    if not owners_search_list and not cats_search_list:
        break

total_movements_to_find_cat = 0
for movements in movements_to_find_a_cat_list:
    total_movements_to_find_cat += movements

if total_movements_to_find_cat:
    avg_movements_find_cat = total_movements_to_find_cat / \
                             len(movements_to_find_a_cat_list)
else:
    avg_movements_find_cat = 0

print 'Total nr of cats:'+str(nr_to_generate)
print 'Nr of cats found:'+str(nr_of_cats_found)
print 'Average number of movements required to find a cat: '+\
      str(avg_movements_find_cat)






